package com.talanthunt.Messages.service;

public interface MessageService {
    
    public void sendFeedbackToCandidate(String text, String email);
}
