package com.talanthunt.seeker.repository;

import java.util.Date;
import java.util.Set;

import com.talanthunt.seeker.model.JobApplication;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobApplicationRespository extends PagingAndSortingRepository<JobApplication, Long> {
    Page<JobApplication> findAll(Pageable pageable);

    Set<JobApplication> findByLastUpdatedDateAndApplicationStatus(Date date, JobApplication.ApplicationStatus applicationStatus);
}
