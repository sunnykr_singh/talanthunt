package com.talanthunt.recruiter.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.talanthunt.seeker.model.JobApplication;

@Entity
public class Recruiter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String companyName;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "recruiter_job_applications", joinColumns = @JoinColumn(name = "recruiter_fk"), inverseJoinColumns = @JoinColumn(name = "application_fk"))
    private List<JobApplication> jobApplications;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "recruiter")
    private List<InterviewSchedule> interviewSchedules;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<JobApplication> getJobApplications() {
        return jobApplications;
    }

    public void setJobApplications(List<JobApplication> jobApplications) {
        this.jobApplications = jobApplications;
    }

    public void addJobApplications(JobApplication jobApplication) {
        if (this.jobApplications == null)
            this.jobApplications = new ArrayList<>(1);
        this.jobApplications.add(jobApplication);
    }

    public List<InterviewSchedule> getInterviewSchedules() {
        return interviewSchedules;
    }

    public void setInterviewSchedules(List<InterviewSchedule> interviewSchedules) {
        this.interviewSchedules = interviewSchedules;
    }

    public void addInterviewSchedules(InterviewSchedule interviewSchedule) {
        if (this.interviewSchedules == null)
            this.interviewSchedules = new ArrayList<>(1);
        this.interviewSchedules.add(interviewSchedule);
    }

}
