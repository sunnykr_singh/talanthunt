package com.talanthunt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class TalanthuntApplication {

	public static void main(String[] args) {
		SpringApplication.run(TalanthuntApplication.class, args);
	}

}
