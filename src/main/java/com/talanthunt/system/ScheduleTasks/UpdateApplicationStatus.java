package com.talanthunt.system.ScheduleTasks;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Set;

import javax.transaction.Transactional;

import com.talanthunt.seeker.model.JobApplication;
import com.talanthunt.seeker.service.JobApplicationService;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UpdateApplicationStatus {

    private JobApplicationService applicationService;
    
    @Scheduled(cron = "0 0 0 * * ?")
    @Transactional
	public void createTarget() {

        Date dateBefore30Days = Date.from(Instant.now().minus(Duration.ofDays(30)));
        Set<JobApplication>  pendingApplications = applicationService.getAllJobApplicationsByDateAndStatus(dateBefore30Days, JobApplication.ApplicationStatus.APPLIED);

        for (JobApplication jobApplication : pendingApplications) {
            jobApplication.setApplicationStatus(JobApplication.ApplicationStatus.DECLINED_BY_SYSTEM);
            applicationService.persistJobApplication(jobApplication);
        }
		
	}
}
