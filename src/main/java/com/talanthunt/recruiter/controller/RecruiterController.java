package com.talanthunt.recruiter.controller;

import javax.websocket.server.PathParam;

import com.talanthunt.recruiter.payload.Schedule;
import com.talanthunt.recruiter.payload.StatusUpdateRequest;
import com.talanthunt.recruiter.service.RecruiterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/recruiter")
public class RecruiterController {

    @Autowired
    public RecruiterService service;

    @RequestMapping(value = "/all-applications", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllJobApplications(@RequestParam(value = "page-size") int pageSize,
            @RequestParam(value = "page-no") int pageNo,
            @RequestParam(value = "from-date", required = false) String fromDate,
            @RequestParam(value = "to-date", required = false) String toDate,
            @RequestParam(value = "status", required = false) String status) {
        try {
            if (fromDate == null && status == null) {
                return new ResponseEntity<>(service.getAllJobApplcation(pageSize, pageNo), HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Opps...! Somthing Went Wrong", HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/download-resume/{applicationId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> downloadResume(@PathParam(value = "applicationId") long applicationId) {
        try {
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Opps...! Somthing Went Wrong", HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/application-status", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateStatus(@RequestBody StatusUpdateRequest updateRequest) {
        try {
            return new ResponseEntity<>(service.updateApplicationStatus(updateRequest), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Opps...! Somthing Went Wrong", HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/schedule-interview", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> scheduleInterview(@RequestBody Schedule schedule) {
        try {
            return new ResponseEntity<>(service.scheduleInterview(schedule), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/all-interview-schedules", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllInterviewSchedules(@RequestParam(value = "page-size") int pageSize,
            @RequestParam(value = "page-no") int pageNo,
            @RequestParam(value = "from-date", required = false) String fromDate,
            @RequestParam(value = "to-date", required = false) String toDate,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "recruiter-id", required = false) Long recruiterId,
            @RequestParam(value = "application-id", required = false) Long aplicationId,
            @RequestParam(value = "sort-by", required = false) String sortBy) {
        try {
            if (recruiterId != null) {
                return new ResponseEntity<>(service.getAllInterviewSchedulesByRecruiter(pageSize, pageNo, recruiterId),
                        HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Opps...! Somthing Went Wrong", HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/available-time-slots", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAvailableTimeSlots(@RequestParam(value = "applicationId") long applicationId,
            @RequestParam(value = "recruiterId") long recruiterId) {
        try {
            return new ResponseEntity<>(service.findAvailableTimeSlots(recruiterId, applicationId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Opps...! Somthing Went Wrong", HttpStatus.EXPECTATION_FAILED);
        }
    }
}
