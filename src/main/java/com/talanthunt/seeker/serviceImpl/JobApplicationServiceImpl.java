package com.talanthunt.seeker.serviceImpl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.talanthunt.seeker.model.Feedback;
import com.talanthunt.seeker.model.JobApplication;
import com.talanthunt.seeker.model.JobApplication.ApplicationStatus;
import com.talanthunt.seeker.payload.JobApplicationRequest;
import com.talanthunt.seeker.repository.FeedbackRepository;
import com.talanthunt.seeker.repository.JobApplicationRespository;
import com.talanthunt.seeker.service.JobApplicationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class JobApplicationServiceImpl implements JobApplicationService {

    @Autowired
    private JobApplicationRespository jobApplicationRespository;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Override
    public String saveResume(MultipartFile resume) {
        // read file in inputstream
        // connect to file server using sftpchanel
        // save file with UUID
        // return file name
        return null;
    }

    @Override
    public long saveJobApplication(JobApplicationRequest formData) {

        JobApplication application = new JobApplication();
        application.setFirstName(formData.getFirstName()).setLastName(formData.getLastName())
                .setEmailId(formData.getEmailId()).setPhoneNumber(formData.getPhoneNumber())
                .setCurrentAddress(formData.getCurrentAddress()).setCurrentCTC(formData.getCurrentCTC())
                .setExpectedCTC(formData.getExpectedCTC()).setYearsOfExperience(formData.getYearsOfExperience())
                .setResume(formData.getResume()).setProfileSummary(formData.getProfileSummary())
                .setApplicationStatus(JobApplication.ApplicationStatus.APPLIED).setLastUpdatedDate(new Date());

        return persistJobApplication(application).getId();
    }

    @Override
    public String getApplicationStatus(long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public JobApplication persistJobApplication(JobApplication application) {
        return jobApplicationRespository.save(application);
    }

    @Override
    public Page<JobApplication> getAllJobApplications(int pageSize, int pageNo) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return jobApplicationRespository.findAll(pageable);
    }

    @Override
    public JobApplication getJobApplication(long id) {
        Optional<JobApplication> optionalData = jobApplicationRespository.findById(id);
        if (optionalData.isPresent())
            return optionalData.get();
        return null;
    }

    @Override
    public Set<JobApplication> getAllJobApplicationsByDateAndStatus(Date date, ApplicationStatus applicationStatus) {

        return null;
    }

    @Override
    public List<Feedback> getMailHistory(long jobApplicationId) {
        return feedbackRepository.findByJobApplicationId(jobApplicationId);
    }

}
