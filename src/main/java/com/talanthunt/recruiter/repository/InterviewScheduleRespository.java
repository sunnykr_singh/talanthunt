package com.talanthunt.recruiter.repository;

import java.util.Date;

import com.talanthunt.recruiter.model.InterviewSchedule;
import com.talanthunt.recruiter.model.Recruiter;
import com.talanthunt.seeker.model.JobApplication;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InterviewScheduleRespository extends PagingAndSortingRepository<InterviewSchedule, Long> {

    public Page<InterviewSchedule> findByRecruiter(Recruiter r, Pageable pageable);

    public Page<InterviewSchedule> findByJobApplication(JobApplication ja, Pageable pageable);

    public Page<InterviewSchedule> findByFromTimeBetween(Date fromTime, Date toTime, Pageable pageable);

}
