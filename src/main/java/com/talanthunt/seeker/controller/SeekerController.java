package com.talanthunt.seeker.controller;

import com.talanthunt.seeker.payload.JobApplicationRequest;
import com.talanthunt.seeker.service.JobApplicationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/seeker")
public class SeekerController {

    @Autowired
    public JobApplicationService jobApplicationService;

    @RequestMapping(value = "/upload-resume", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> uploadResume(@RequestBody MultipartFile file) {
        try {
            return new ResponseEntity<>(jobApplicationService.saveResume(file), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Opps...! Somthing Went Wrong", HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> registerSeeker(@RequestBody JobApplicationRequest applicationRequest) {
        try {
            return new ResponseEntity<>(jobApplicationService.saveJobApplication(applicationRequest), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Opps...! Somthing Went Wrong", HttpStatus.EXPECTATION_FAILED);
        }
    }
}
