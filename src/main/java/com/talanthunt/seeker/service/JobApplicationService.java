package com.talanthunt.seeker.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.talanthunt.seeker.model.Feedback;
import com.talanthunt.seeker.model.JobApplication;
import com.talanthunt.seeker.payload.JobApplicationRequest;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Defines Services for seeker to submit/retrieve job application
 */
@Service
public interface JobApplicationService {

    /**
     * uploads file to file server
     * 
     * @param resume pdf file
     * @return file name
     */
    public String saveResume(MultipartFile resume);

    public String getApplicationStatus(long id);

    public JobApplication getJobApplication(long id);

    public long saveJobApplication(JobApplicationRequest application);

    public JobApplication persistJobApplication(JobApplication application);

    public Page<JobApplication> getAllJobApplications(int pageSize, int pageNo);

    public Set<JobApplication> getAllJobApplicationsByDateAndStatus(Date date,
            JobApplication.ApplicationStatus applicationStatus);

    public List<Feedback> getMailHistory(long jobApplicationId);

}
