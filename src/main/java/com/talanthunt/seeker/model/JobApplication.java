package com.talanthunt.seeker.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.talanthunt.recruiter.model.InterviewSchedule;
import com.talanthunt.recruiter.model.Recruiter;

@Entity
public class JobApplication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    public String firstName;

    public String lastName;

    public String emailId;

    public String phoneNumber;

    public String currentAddress;

    public String resume;

    @Lob
    public char[] profileSummary;

    public float yearsOfExperience;

    public String currentCTC;

    public String expectedCTC;

    @Enumerated(EnumType.STRING)
    public ApplicationStatus applicationStatus;

    @Temporal(TemporalType.DATE)
    public Date lastUpdatedDate;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "jobApplication")
    public List<Feedback> feedbacks;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "jobApplication")
    public List<InterviewSchedule> interviewSchedules;

    @JsonIgnore
    @ManyToMany(mappedBy = "jobApplications", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<Recruiter> appliedTo;

    public static enum ApplicationStatus {
        APPLIED, SHORTLISTED, SELECTED, DECLINED_BY_SYSTEM, REJECTED
    }

    public long getId() {
        return id;
    }

    public JobApplication setId(long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public JobApplication setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public JobApplication setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmailId() {
        return emailId;
    }

    public JobApplication setEmailId(String emailId) {
        this.emailId = emailId;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public JobApplication setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public JobApplication setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
        return this;
    }

    public String getResume() {
        return resume;
    }

    public JobApplication setResume(String resume) {
        this.resume = resume;
        return this;
    }

    @Lob
    public char[] getProfileSummary() {
        return profileSummary;
    }

    public JobApplication setProfileSummary(char[] profileSummary) {
        this.profileSummary = profileSummary;
        return this;
    }

    public float getYearsOfExperience() {
        return yearsOfExperience;
    }

    public JobApplication setYearsOfExperience(float yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
        return this;
    }

    public String getCurrentCTC() {
        return currentCTC;
    }

    public JobApplication setCurrentCTC(String currentCTC) {
        this.currentCTC = currentCTC;
        return this;
    }

    public String getExpectedCTC() {
        return expectedCTC;
    }

    public JobApplication setExpectedCTC(String expectedCTC) {
        this.expectedCTC = expectedCTC;
        return this;
    }

    public ApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public JobApplication setApplicationStatus(ApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
        return this;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public JobApplication setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
        return this;
    }

    public List<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public JobApplication setFeedbacks(List<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
        return this;
    }

    public void addFeddback(Feedback feedback) {
        if (this.feedbacks == null)
            this.feedbacks = new ArrayList<>(1);
        this.feedbacks.add(feedback);
    }

    public List<InterviewSchedule> getInterviewSchedules() {
        return interviewSchedules;
    }

    public void setInterviewSchedules(List<InterviewSchedule> interviewSchedules) {
        this.interviewSchedules = interviewSchedules;
    }

    public Set<Recruiter> getAppliedTo() {
        return appliedTo;
    }

    public void setAppliedTo(Set<Recruiter> appliedTo) {
        this.appliedTo = appliedTo;
    }

}
