package com.talanthunt.recruiter.payload;

import java.util.List;

import com.talanthunt.seeker.model.JobApplication;

public class JobApplicationsResponse {
    private List<JobApplication> jobApplications;
    private int totalNumberPages;
    private String message;

    public List<JobApplication> getJobApplications() {
        return jobApplications;
    }

    public void setJobApplications(List<JobApplication> jobApplications) {
        this.jobApplications = jobApplications;
    }

    public int getTotalNumberPages() {
        return totalNumberPages;
    }

    public void setTotalNumberPages(int totalNumberPages) {
        this.totalNumberPages = totalNumberPages;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
