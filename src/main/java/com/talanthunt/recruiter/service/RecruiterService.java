package com.talanthunt.recruiter.service;

import java.util.List;

import com.talanthunt.recruiter.model.InterviewSchedule;
import com.talanthunt.recruiter.payload.JobApplicationsResponse;
import com.talanthunt.recruiter.payload.Schedule;
import com.talanthunt.recruiter.payload.StatusUpdateRequest;
import com.talanthunt.recruiter.payload.TimeSlot;
import com.talanthunt.seeker.model.JobApplication;

import org.springframework.data.domain.Page;

/**
 * Recruiter Service provides features for recruiters such as
 * 
 * - updating any job application status - fetch all job applications add
 * filters such as application status and date range - download resume - fetch
 * complete job application details
 */
public interface RecruiterService {

    /**
     * finds application by id set new status, adds feedback and send email to
     * candidate
     * 
     * @param updateRequest object contains application id, new status, and feedback
     *                      to pass on
     * @return current status of job application
     */
    public String updateApplicationStatus(StatusUpdateRequest updateRequest);

    public JobApplicationsResponse getAllJobApplcation(int pageSize, int pageNo);

    public JobApplicationsResponse getAllJobApplcation(int pageSize, int pageNo, String fromDate, String toDate);

    public JobApplication getJobApplication(long applicationId);

    public InterviewSchedule scheduleInterview(Schedule schedule) throws Exception;

    public Page<InterviewSchedule> getAllInterviewSchedulesByRecruiter(int pageSize, int pageNo, long recruiterId);

    public List<TimeSlot> findAvailableTimeSlots(long recruiterId, long jobApplicationId);

}
