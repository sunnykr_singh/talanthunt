package com.talanthunt.recruiter.payload;

public class Schedule {

    private long recruiterId;

    private long jobApplicationId;
    
    private TimeSlot timeSlot;

    public long getRecruiterId() {
        return recruiterId;
    }

    public void setRecruiterId(long recruiterId) {
        this.recruiterId = recruiterId;
    }

    public long getJobApplicationId() {
        return jobApplicationId;
    }

    public void setJobApplicationId(long jobApplicationId) {
        this.jobApplicationId = jobApplicationId;
    }

    public TimeSlot getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(TimeSlot timeSlot) {
        this.timeSlot = timeSlot;
    }
}
