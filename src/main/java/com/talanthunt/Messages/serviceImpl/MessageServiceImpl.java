package com.talanthunt.Messages.serviceImpl;

import com.talanthunt.Messages.service.MessageService;

import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {

    @Override
    public void sendFeedbackToCandidate(String text, String email) {
        System.out.println("sending email to " + email);
    }

}
