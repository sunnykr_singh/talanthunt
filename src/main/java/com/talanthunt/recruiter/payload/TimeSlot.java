package com.talanthunt.recruiter.payload;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TimeSlot {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a", timezone = "IST")
    private Date fromTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a", timezone = "IST")
    private Date toTime;

    public TimeSlot(Date fromTime, Date toTime) {
        this.fromTime = fromTime;
        this.toTime = toTime;
    }

    public TimeSlot() {
    }

    public Date getFromTime() {
        return fromTime;
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }

    public Date getToTime() {
        return toTime;
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }

    @Override
    public String toString() {
        return "TimeSlot [fromTime=" + fromTime + ", toTIme=" + toTime + "]";
    }
}
