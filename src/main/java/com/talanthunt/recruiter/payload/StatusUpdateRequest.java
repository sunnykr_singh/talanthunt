package com.talanthunt.recruiter.payload;

public class StatusUpdateRequest {
    public long applicationId;
    public String status;
    public String feedback;
    public Long assinedTo;

    public long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(long applicationId) {
        this.applicationId = applicationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Long getAssinedTo() {
        return assinedTo;
    }

    public void setAssinedTo(Long assinedTo) {
        this.assinedTo = assinedTo;
    }

}
