package com.talanthunt.recruiter.serviceImpl;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.talanthunt.Messages.service.MessageService;
import com.talanthunt.recruiter.model.InterviewSchedule;
import com.talanthunt.recruiter.model.Recruiter;
import com.talanthunt.recruiter.payload.JobApplicationsResponse;
import com.talanthunt.recruiter.payload.Schedule;
import com.talanthunt.recruiter.payload.StatusUpdateRequest;
import com.talanthunt.recruiter.payload.TimeSlot;
import com.talanthunt.recruiter.repository.InterviewScheduleRespository;
import com.talanthunt.recruiter.repository.RecruiterRepository;
import com.talanthunt.recruiter.service.RecruiterService;
import com.talanthunt.seeker.model.Feedback;
import com.talanthunt.seeker.model.JobApplication;
import com.talanthunt.seeker.service.JobApplicationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class RecruiterServiceImpl implements RecruiterService {

    @Autowired
    private JobApplicationService jobApplicationService;

    @Autowired
    private RecruiterRepository recruiterRepository;

    @Autowired
    private InterviewScheduleRespository interviewScheduleRespository;

    @Autowired
    private MessageService messageService;

    @Override
    public String updateApplicationStatus(StatusUpdateRequest updateRequest) {
        JobApplication application = getJobApplication(updateRequest.getApplicationId());
        if (application != null) {
            application.setApplicationStatus(JobApplication.ApplicationStatus.valueOf(updateRequest.getStatus()));
            if (updateRequest.getStatus().equals(JobApplication.ApplicationStatus.SHORTLISTED.toString())) {
                // if candidate is shortlisted the update request should contain recruiter id
                Recruiter recruiter = getRecruiter(updateRequest.getAssinedTo());
                application.getAppliedTo().add(recruiter);
                recruiter.getJobApplications().add(application);
            }
            Date today = new Date();
            application.setLastUpdatedDate(today);
            if (updateRequest.getFeedback() != null && StringUtils.hasText(updateRequest.getFeedback())) {
                Feedback feedback = new Feedback();
                feedback.setAddedDate(today);
                feedback.setFeedback(updateRequest.getFeedback().toCharArray());
                feedback.setJobApplication(application);
                application.addFeddback(feedback);
                messageService.sendFeedbackToCandidate(updateRequest.getFeedback(), application.getEmailId());
            }
            application = jobApplicationService.persistJobApplication(application);
            return application.getApplicationStatus().toString();
        }
        return null;
    }

    @Override
    @Transactional
    public JobApplicationsResponse getAllJobApplcation(int pageSize, int pageNo) {
        Page<JobApplication> pagedData = jobApplicationService.getAllJobApplications(pageSize, pageNo);
        JobApplicationsResponse response = new JobApplicationsResponse();
        response.setJobApplications(pagedData.getContent());
        response.setTotalNumberPages(pagedData.getTotalPages());
        response.setMessage("Data Fetched succesfully");
        return response;
    }

    @Override
    public JobApplicationsResponse getAllJobApplcation(int pageSize, int pageNo, String fromDate, String toDate) {
        return null;
    }

    @Override
    public JobApplication getJobApplication(long applicationId) {
        return jobApplicationService.getJobApplication(applicationId);
    }

    @Override
    @Transactional
    public InterviewSchedule scheduleInterview(Schedule schedule) throws Exception {
        JobApplication application = getJobApplication(schedule.getJobApplicationId());
        Recruiter recruiter = getRecruiter(schedule.getRecruiterId());

        for (InterviewSchedule interviewSchedule : recruiter.getInterviewSchedules()) {
            if (interviewSchedule.getFromTime().getTime() == schedule.getTimeSlot().getFromTime().getTime())
                throw new Exception("Recruiter not available");
        }

        for (InterviewSchedule interviewSchedule : application.getInterviewSchedules()) {
            Date oneHourBefore = Date.from(Instant.ofEpochMilli(schedule.getTimeSlot().getFromTime().getTime())
                    .atZone(ZoneId.systemDefault()).minusHours(1).toInstant());
            if (interviewSchedule.getFromTime().getTime() == schedule.getTimeSlot().getFromTime().getTime()
                    || interviewSchedule.getFromTime().getTime() == oneHourBefore.getTime())
                throw new Exception("Candidate not available");
        }

        InterviewSchedule newSchedule = new InterviewSchedule();
        newSchedule.setJobApplication(application);
        newSchedule.setRecruiter(recruiter);
        newSchedule.setFromTime(new Date(schedule.getTimeSlot().getFromTime().getTime()));
        newSchedule.setToTime(new Date(schedule.getTimeSlot().getToTime().getTime()));
        newSchedule = interviewScheduleRespository.save(newSchedule);
        recruiter.getInterviewSchedules().add(newSchedule);
        application.getInterviewSchedules().add(newSchedule);
        return newSchedule;
    }

    @Override
    public Page<InterviewSchedule> getAllInterviewSchedulesByRecruiter(int pageSize, int pageNo, long recruiterId) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(Direction.ASC, "fromTime"));

        return interviewScheduleRespository.findByRecruiter(getRecruiter(recruiterId), pageable);
    }

    @Override
    public List<TimeSlot> findAvailableTimeSlots(long recruiterId, long jobApplicationId) {

        return null;
    }

    private Recruiter getRecruiter(long recruiterId) {
        Optional<Recruiter> optionalData = recruiterRepository.findById(recruiterId);

        if (optionalData.isPresent())
            return optionalData.get();
        return null;
    }

}
