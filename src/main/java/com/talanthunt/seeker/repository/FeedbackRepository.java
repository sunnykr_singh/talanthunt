package com.talanthunt.seeker.repository;

import java.util.List;

import com.talanthunt.seeker.model.Feedback;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, Long> {

    @Query("SELECT f FROM Feedback f JOIN f.jobApplication ja WHERE ja.id = :applicationId")
    List<Feedback> findByJobApplicationId(@Param(value = "applicationId") long applicationId);
}
